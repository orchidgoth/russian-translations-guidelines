General Translation Guidelines 
==============================
## 1. People's Names
People's names are transliterated but not translated. 

Even if the name in English version of materials has an Eastern European equivalent (i.e. in Social Engineering video the girl named Lydia) - transliterate but do not translate. 
Do not use the shortened/informal versions of names that have a Russian equivalent. (i.e. don't call "Lydia" "Лида", the translation would be "Лидия". 

## 2. Infosec-Specific Terms
Transliterating and not translating is often the best way to handle specific and newer terms. (i.e. "phishing" would be the transliteration, фишинг).

If a term has been made up of two words, one of which has a Russian equivalent - it would make sense to transliterate the borrowed-from-another-language part and translate the one that has a widely used equivalent. (i.e. "cryptocurrency": the "crypto" part gets transliterated, while "currency" that is "валюта" in Russian is translated.) 

Terms that are less specific to infosec and are/were used in other ares of IT industry can be translated according to the wording that is common. 

IT and infosec terms used by people outside of the industry can be translated as the common slang words (i.e. "IT" or "IT department" can be translated as "айтишники") if: 
     * the materials containing those are aimed on a general (non industry-specific) audience 
     * style of presentation is informal and would benefit from using those words by being "closer to the audience"
     * using slang words does not affect the accuracy of the term (don't call "IT Department" программисты as they aren't programmers (though some of them might be!)
     * do not use slang-like words in any official documentation translations (the said "IT department" would be "IT-отдел in this case. 
  
  An infosec term that involves reference to human qualities (i.e. "social engineer" that can be both a job and a personality type that can succeed in this task) can have multiple translations based on context. (see also: "social engineering"). 
  
  
  "Buzzwords", terms media pick up and often misinterpret or translate inaccurately:
     * can be translated with the same word used in the media, if it's widespreadness is more significant compared to accuracy; watch the context carefully!
     * should be translated in a creative (not word-by-word) way if the inaccuracy is critical and contributes to spreading wrong info through use of the inaccurately translated term. 
     * check on which word the target audience is likely to use to refer to a thing, and how accurate that word represents it. 
## 3. Abbreviations
The mass-known ones (or easy to look up and find out meaning without too much research) can be left as is. 
Infosec-specific and therefore not known by audiences out of the industry: 
    * leave as is **and** provide the target language equivalent at first instance of the abbreviation
    * next time the abbreviation appears - leave as is, **or** link to the initial mention with translation (in text materials)
    * if there is a known or established equivalent abbreviation in target language - check on frequency of use and if it is the common one to refer to a thing - use it. 
     * Do not invent your own abbreviations using the same principle as has been applied to create the English one

## 4. Social Media Names
Do not transliterate or translate social media names. Twitter, Facebook, Instagram and others, including the new ones, should be kept in English. 

However, if there is a commonly used transliteration or even slang word for social media **popular among target language users**, it's ok to use it. (i.e. Фейсбук, Инста/Инстаграм, ВК, Твиттер etc. )
Avoid transliterating names of new social media or those not common for the target language speakers.


## 5. On-screen text (on phone, laptop, someone's shirt etc.)
Translate in subtitles only if it's not clear without that. 
Otherwise leave as is. 


